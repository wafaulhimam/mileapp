### Frontend Developer (Answers)
#### 1. Do you prefer vuejs or reactjs? Why?
Vue andd react are two frontend JS frameworks that popular between developer. Both has tools that can make developer easily work with it, but each has its own best uses cases, pros, and cons. So many similarities between both them, such as dom manipulation, focus on library view, etc.

But we can spot the differences, what devs loves -like me- from Vue is that it has easily learning curve, has good documentation, lightweight. From react maybe need some time to learn, such as 3rd party routing or state management (redux thunk, saga, etc), has huge package ecosytem, widespread usage, and has corporate backing (facebook).

From this we can have conclusion that no one better from another. Just we need to know what we build. If we build only lightwight app, we can use Vue (But not saying vue is bad for big-scaling app), and when we need bigger app, we need to consider using React from beginning. 

#### 2. What complex things have you done in frontend development?
Nowadays frontend development really develop very fast. In old days we need to consider only HTML, CSS, and some Javascript code. But we now facing many different types of devices and browsers. Complex thing is come from making an app that cross-platform & cross-browser that can have 'same' good user interface and user experience. This is a challenge for devs -like me- developing frontend field.

#### 3. Why does a UI Developer need to know and understand UX? how far do you understand it?
UI devs need to know and understand UX the best. We can make an analogy from a sauce bottle. According to the good UI, the shape of the bottle is straight up with cap on top, with a glossy glass and an attractive logo. Meanwhile, according to the UX side, it is the best to place bottle upside down with a cap on the bottom with a soft plastic container, as it make easier for the user to put out the sauce from bottle.

With that analogy, we need to think what the need of user with good UI in the mind.

#### 4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
I'll have some analysis regarding UI & UX in that page, such below:

* ipad-portrait

![ipad-portrait](ipad-portrait.png)

As we can see in this page, for the UI side, the Copyright text position is not really good, and the color not really suitable with red things, this not good for UI side.

For section 'Call Us Now' and change language, dont have here, unlike on the web page (when we access using desktop), this will affect for User Experrience.

* iphone5-portrait

![iphone5-portrait](iphone5-portrait.png)

For the UI thing, copyright already have good position in the center, but we need to scroll down to lookup all of this page. I think better to put all content, since not many displayed here. I think is not necessary for user to scroll down 'only' to lookup for Copyright.

Also same with ipad portrait view, for 'call us now' and change language should be visible here.

And this applied for all of mobile view.

#### 5. Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com!
The deployed web you can see [here](https://cocky-heyrovsky-ca9843.netlify.app/). 
The source code you can found [here](https://github.com/wafaulhimam/mileapp-login).

#### 6. Solve the logic problems below!
a. Swap the values of variables A and B

```javascript
let A = 3;
let B = 5;

[A, B] = [B, A];
console.log(A, B);
```

b. Find the missing numbers from 1 to 100

```javascript
const missingNumbers = (a, l=true) => Array.from(Array(Math.max(...a)).keys()).map((n, i) => a.indexOf(i) < 0  && (!l || i > Math.min(...a)) ? i : null).filter(f=>f);

const numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];

console.log(missingNumbers(numbers));
```

c. Return the number which is called more than 1

```javascript
const findDuplicates = arr => arr.filter((item, index) => arr.indexOf(item) != index);

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];

console.log([...new Set(findDuplicates(numbers))]);
```

d. array into object

```javascript
const array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."];

const newObj = array_code.reduce((accumulator, currentValue) => {
  accumulator[currentValue] = currentValue;
  return accumulator;
}, {});

console.log(newObj);
```